<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <title>Лаба</title>

</head>

<body>

  <div class="content">
    <div class="container">
      <div class="content__wrapper">

        <div class="content-item__form">

          <?php
            if (!empty($messages)) {
              print('<div id="messages">');
              foreach ($messages as $message) {
                print($message);
              }
              print('</div>');
            }
          ?>


          <!--FORM-->
          <form id="form" action="" method="POST">
            <label>
              Введите имя
              <br />
              <input type="text" name="name" 
                <?php if ($errors['name']) {print "class='error'" ;} ?> 
                value="<?php print $values['name']; ?>"
                placeholder="Вадим">
            </label>
            <br />

            <label>
              Введите eMail
              <br />
              <input type="email"  name="email"
                <?php if ($errors['email']) {print 'class="error"' ;} ?> 
                value="<?php print $values['email']; ?>" 
                placeholder="gg@mail.ru">
            </label>
            <br />

            <label>
              Выберите год рождения
              <br />
              <input type="date" name="birthYear" 
                <?php if ($errors['birthYear']) {print 'class="error"' ;} ?> 
                value="<?php print $values['birthYear']; ?>"
                value="2019-08-13">
            </label>
            <br />

            Выберите пол
            <br />
            <label>
              <input type="radio" name="sex" 
                <?php if ($errors['sex']) {print 'class="error"' ;} 
                if($values['sex']=="М") {print "checked='checked'";}?>
                value="M">Муж
            </label>
            <label>
              <input type="radio" name="sex" 
                <?php if ($errors['sex']) {print 'class="error"' ;} 
                if($values['sex']=="Ж"){print "checked='checked'";}?>
                value="F">Жен
            </label>
            <br />

            Выберите кол-во конечностей
            <br />
            <label>
              <input type="radio" name="countlimbs" 
                <?php 
                  if ($errors['countlimbs']) {print 'class="error"' ;} 
                  if($values['countlimbs']=="4"){print "checked='checked'";}
                ?>
                value="2,5">2,5
            </label>
            <label>
              <input type="radio" name="countlimbs"
                <?php if ($errors['countlimbs']) {print 'class="error"' ;} 
                if($values['countlimbs']=="8"){print "checked='checked'";}?> 
                value="3">3
            </label>
            <label>
              <input type="radio" name="countlimbs" 
                <?php if ($errors['countlimbs']) {print 'class="error"' ;} 
                if($values['countlimbs']=="22"){print "checked='checked'";}?>
                value="4">4
            </label>
            <br />

            <label>
              Выберите свои сверхспособности
              <br />
              <select name="superpower" multiple="multiple">
                <option value="бессмертие" <?php if($values['superpower']=="Бессмертие"){print "selected='selected'";}?>>бессмертие</option>
                <option value="прохождение сквозь стены" <?php if($values['superpower']=="Прохождение сквозь стены"){print "selected='selected'";}?>>прохождение сквозь стены</option>
                <option value="левитация" <?php if($values['superpower']=="Левитация"){print "selected='selected'";}?>>левитация</option>
              </select>
            </label>
            <br />

            <label>
              Биография
              <br />
              <textarea name="biography" placeholder="начальное значение" <?php if ($errors['biography']) {print 'class="error"' ;} ?><?php print $values['biography']; ?>></textarea>
            </label>
            <br />

            <label>
              С контактом ознакомлен
              <input type="checkbox" name="check1" <?php  if($values['check1']=="1"){print "checked='checked'";}?> value="1">
            </label>
            <br />

            <input id="input" type="submit">
          </form>
          <!--/FORM-->

        </div>

      </div>
    </div>

  </div>

  
</body>

</html>
