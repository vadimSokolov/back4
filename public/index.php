<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['birthYear'] = !empty($_COOKIE['birthYear_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['countlimbs'] = !empty($_COOKIE['countlimbs_error']);
  $errors['superpower'] = !empty($_COOKIE['superpower_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check1'] = !empty($_COOKIE['check1_error']);
  $errors['correctName'] = !empty($_COOKIE['correctName_error']);
  $errors['correctEmale'] = !empty($_COOKIE['correctEmale_error']);
  $errors['correctBirthYear'] = !empty($_COOKIE['correctBirthYear_error']);

  if ($errors['name']) {
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Заполните e-mail.</div>';
  }
  if ($errors['birthYear']) {
    setcookie('birthYear_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['countlimbs']) {
    setcookie('countlimbs_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['superpower']) {
    setcookie('superpower_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['check1']) {
    setcookie('check1_error', '', 100000);
    $messages[] = '<div class="error">Примите условия.</div>';
  }
  if ($errors['correctName']){
    setcookie('correctName_error', '', 100000);
    $messages[] = '<div class="error">Имя должно быть написано при помощи латинского алфавита без добавления лишних знаков</div>';
  } 
  if ($errors['correctEmale']){
    setcookie('correctEmale_error', '', 100000);
    $messages[] = '<div class="error">Формат эл почты имеет вид: example@email.com</div>';
  } 
  if ($errors['correctBirthYear']){
    setcookie('correctBirthYear_error', '', 100000);
    $messages[] = '<div class="error">Формат даты 02.02.2021</div>';
  } 

  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['birthYear'] = empty($_COOKIE['birthYear_value']) ? '' : $_COOKIE['birthYear_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['countlimbs'] = empty($_COOKIE['countlimbs_value']) ? '' : $_COOKIE['countlimbs_value'];
  $values['superpower'] = empty($_COOKIE['superpower_value']) ? '' : $_COOKIE['superpower_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['check1'] = empty($_COOKIE['check1_value']) ? '' : $_COOKIE['check1_value'];

  include('form.php');
}

else {
  $errors = FALSE;
  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['name'])){
    setcookie('correctName_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['email'])){
    setcookie('correctEmale_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['birthYear'])){
    setcookie('correctBirthYear_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['name'])) {
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['birthYear'])) {
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('birthYear_value', $_POST['birthYear'], time() + 365 * 24 * 60 * 60);
  } 
  if (empty($_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['countlimbs'])) {
    setcookie('countlimbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('countlimbs_value', $_POST['countlimbs'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['superpower'])) {
    setcookie('superpower_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('superpower_value', $_POST['superpower'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['biography'])) {
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['check1'])) {
    setcookie('check1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check1_value', $_POST['check1'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('correctName_error', '', 100000);
    setcookie('correctEmale_error', '', 100000);
    setcookie('correctBirthYear_error', '', 100000);
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birthYear_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('countlimbs_error', '', 100000);
    setcookie('superpower_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('check1_error', '', 100000);
  }

  $name = $_POST['name'];
  $email = $_POST['email'];
  $birthYear = $_POST['birthYear'];
  $sex = $_POST['sex'];
  $countlimbs = $_POST['countlimbs'];
  $superpower = $_POST['superpower'];
  $biography = $_POST['biography'];
  $check1 = $_POST['check1'];
  
  $user = 'u24098';
  $pass = '23463546';
  $db = new PDO('mysql:host=localhost;dbname=u24098', $user, $pass);
  
  try {
    $stmt = $db->prepare("INSERT INTO form (name,email,birthYear,sex,countlimbs,superpower,biography,check1) VALUE (:name,:email,:birthYear,:sex,:countlimbs,:superpower,:biography,:check1)");
    $stmt -> execute(['name'=>$name,'email'=>$email,'birthYear'=>$birthYear,'sex'=>$sex,'countlimbs'=>$countlimbs,'superpower'=>$superpower,'biography'=>$biography,'check1'=>$check1]);
    echo "<script type='text/javascript'>alert('Спасибо, результаты сохранены.');</script>";
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }

  setcookie('save', '1');

  header('Location: index.php');
}

